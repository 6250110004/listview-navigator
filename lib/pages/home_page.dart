import 'package:flutter/material.dart';
import 'package:fruit_test/model/fruit_data_model.dart';
import 'package:fruit_test/pages/fruit_detail.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  static List<String> fruitname = [
    'Apple',
    'Banana',
    'Mango',
    'Orange',
    'Pineapple',
  ];

  static List url = [
    'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBIWFRYREhUYEhUYGBgSGBIYEhIRGhESGRUZGRgYGBgcIS4lHB4rIRgYJjgmKzAxNTU1GiQ7QDs1Py40NTEBDAwMEA8QHhISHjEkISs0NDQ0NDQ0NDQxNDQ0NDQ0NDQ0NDQxNDQ0MTQ0MTQ0NDQ0NDQ0NDQ0NDE0NDQ0NDQ0NP/AABEIAPgAywMBIgACEQEDEQH/xAAbAAABBQEBAAAAAAAAAAAAAAAAAgMEBQYBB//EAEAQAAIBAgMGAwUGAwUJAAAAAAABAgMRBCExBQYSQVFhcYGREyKhscEHMmLR4fBCcpIUI1Ki8RYXM1RzgpPCw//EABoBAAIDAQEAAAAAAAAAAAAAAAACAQMEBQb/xAAoEQACAgEEAQIHAQEAAAAAAAAAAQIRAwQSITFBE1EUIkJSYXGBBTL/2gAMAwEAAhEDEQA/APZgAAAAAAA4Zjbu+WGw74INYipxOMoQmvcte/FKzSd8rajG+235UY+wpOUas0pe0SSVOnxWdm1m3ZrLTW6yPNnBttttttttttybd223q31M+TNtdIVst9o7342qrcfso8XEvZ8VOSWaSclLNZ/AqKu0MRKTnKtUcnk5e0mm7K2dn0D2fquXVCHHny5ozubfkgk4Da+KpNeyrzjbSHG5xf8A2SvH4G12Hv7F2ji4qLeSrRT4b/ihm4+Kvrokefyhy80wU+b55S8eUl3/AF6kxySj0Mke8UqiklKLUk1dSTUlJPRprVDh5HuxvLPDS4ZXlRb9+F78N/44dH1Wj87r1fDYiE4qpBqUZJOMlo0zXjyKS/JI8AAWAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcIO2cZ7KhUqppOMXwtq643lHLnm0TjH7/4hqNKktJOU33cUkl3+8/RFeWW2LYGErSlKUpzblKTvKTd3JifZ5X5d8vRvIev29cxufr8dP9Wc2yuhEorS+eXC0n1G5Wd2l4rJea8xySzXkI/fkCYEeay0+OnwGppa5/AlSiMSiNY8WMcVuf6roa/cXeVUZf2arL+6m/dk3lTm/lF8+jz6mUjRlJqEIucnpGKcpN9ks2XWB3Fx1XNwjRi871JcL8oxTkn2aRZDduuJbw+z2QDzzalLauz8JKrDFwxEafAvZzw7bjFyUfdnx3drrJ8ug3u99pfHHhxVJRmv4qcklPwjJ5f1M270u+AWOTVrk9HAr9k7Up4in7WlxWu4+9FxaktV0firosBkxGqOgAEkAAAAAAAAAAAAAAAAAAAAAM18PCceGcYzXSUVJejHgADMY7c7DzfFCUqX4Uk436pPNepQV9zMSpPgdOUbuz45J25X93U9FEyklm8il4IPwRVnmNbdPGppKmp5XvGcLLt7zWYUtz8a9YRh/NUj/wCrZu8Zt7DU/vTT7LP46FRW37wsdFN+SX1K3gxryyyOmnLqLKqhuFVf360I/wAsZVPnwlrhNxMLHOpKdV9HLgj6Rz+Ihb/Yb/BU9I/mSKe++EevHHxivzGjDEiz4TMvpZeYPAUaS4aVONNc+GKjfxa18yWUtDebCS0qJeMWixo4ylP7k4y8JIuTj4K5Y5x7TRj/ALWMeoYNUk/eq1Ixt+CPvN+vD6mI3L3fWIqwi/ur359oLX1yXmTvtSxntcbTw6d40oK6/HP35f5eA2u4Oz40sP7V2TqO99LQjdL48T9DPL5slGiLcMVrtmooUYwioQSjGKSUUrJJckOEWttCjH704rzv8isxG9OFj/E5eC/M0bkvJRHFOXSbL4DH1d+qK0g352+gmnv3Sbzg15/oR6kfcs+EzfabICkwW8mHqc3F91+RcU6kZK8WmuqdxlJPoplCUeJKhwAAkUAAAAAAAAAAAA4AFTtPaHDeMXbrLp2RDdDRi5OkObQ2pGneK96XTkvF/Qy+PxVerq3blFZJeRHxu06cNXd9CkxO8r0jkuxXKZvxYHHpX+RO0cBWekJy8ItlXSwM+K04uL6NNEmG36jf3n6j1XbDkrt3tzuUujdB5FxRNwexeLkWMd2exmIb0V4P+7kvOKYqW9WMlrUsukYxj9CFQzx5+00kXuI2NGGdyvnipU37l34XIUdqzec5yl4yYv8A2jUPu6kWLU+nyVKo1q2KnUdOcrvX2c2uS1t2LyrtfGRioKhUjCK4V/d1F7qyXIhVd9MR/A7IaW+OKesr+ZNK7EhilHtJjOJ2nVf34yj4qS+ZGWKbLSO9M5ffipeKTHVtXDz/AOJSj48KXyFbRrjuXgpZVmI9sy1xEMLLOCa7cTJ+znhoWfBCT/EuP5i2hmp1dFPgdoSi9Tb7E2xLLNrzscjtShZJ0qdv+nH8h6licNLSEYP8K4S2LSfZjyxlNfNE1OF2qnZS/qX1RaQmmrp3XUx1OpD+FkzCY+UH1jzRoUjnZNP9ppwGMPXjNKUXdfJ9GPjmNquGdAAADgAMYvEKEXOWiVwJSt0iFtjaMaUXnZ2zfRfmeZbc3mu3CnoRd7d4ZVJSSfMzFOnKWdn4lUnZ2NNp1FfMScTjJSd7vzZHdR+It07DcolbOhSS4OqrnfTt0FuqyPY6LRCSHOIV7WwxK+vU5x5NfHoFDNjk676jMpiWzhNFbYNiuJcv2xpsVD0CgT5JVLPnYdgyPAfgI0XxJUFlfLW1r5+nQl4ZkSgixow0Vv1Fodyrgkxl18vEQ6j5EmOCm1ewPAT6C0L6kV5Gqe0KkeZb4HbN/dmVFXCS6aZaW9SOk4voTFuL4IlHHkXK5PQtl4/hlxRd4vWPVGtp1FJKSzTzR5JgMa4vU3e720U/cb107P8AU1wlfBxdbpXH5kaQDh0tOYcMJ9om2uCHsYvPWXnp8PmbavUUYym9EnJ+CR4hvVi5Vq0nreT+YsnxRq0kN09z8FFg8PKtO70vc0H9nUVZLIe2ZglCCyzY5ViIdWypxNNWeWfXoV84ltiIldNvNcnqLJF8XwRJDbY5Njc5XzIoY42IbBs5xuzXUKFbONnOIS5CXIKFbFNknB4Zz06pa559EQrmj2DSV0DRXOW1WWuzd3HNXsQ9q7HlSeasenbuRVlkI3zwUZQUkkpJ+qFlDizNi1cvUUX0eYYGg2zU7K2XxSSsQcFhbSPQdgYJJKTX+pEI3wX6vO4q0O0thwUUnrYJbDjysXPEc4jRsicj1snuZjE7C7Ga2rshxu7HpTkQNpYWM4vLMSeJVwaMOqnGSs8jmnBl3sjaPC11ve5G23h1ByVvB9Cvwc7MzptHd2xyY7Z7Hs/FKpBTXPXs1qSzH7m43N0m9VxLxWvw+RsDZGW5WeZ1GL08jiUW92L4KEktZe75LNnlOFw3HU4n1ub3fyvdxp9Ffzf6WMxs2hZOQsuzbpo7YX7jsokWu3a3Im1UQqvoKakVtcrJpZ52tp3LOuVVYC6JEmxlj0xtOzva/YgsGmIY5IQ0ArEMHFWvfPoKaE2CxRKNNsR5ozaRe7LqWsLJlc42j1LYWIskSN4a94WMrs7H2SzJeOxynGzdu/QTfxRmhgampELDT943uyq69mjzalVtLW+evU1eAx9oJXGg6Zbq8bkka1VTkqpVUMYmtRU8UWbznek7J06w26/Iq6mKI7xncjeWRwMpd6KObZl8Nq7u1k34voa7bc1OBjXkyh9nc0lvHTNHsHGcFWEr2Skr/wAryl8Gz1NM8Zwc8z1rZ1ZypU59YRb8bZmjC+0cv/Tx1JS/hiN6p8dZro+H0y+hFpwtGxJ2suKrJ/ifzG5LIH2GPiKRErECuT6pArkF8SvxLXJWy8bsrK6LOuivxESS+KIE0NNEiaG5IWxxloS4ZXvztYdaESRFijTRxocaEtEWFCYQbdlmydhZ2ISJFJismMbLyhimuZIli20UsJjymVUXxgiwp18y0o46y1M5GY9GqyxBLEpGrw21bcyatp8V7Z2V32Ri41WORqy5NrlqQxfhIs089pJ8xiWP7lBxvqdU5Z9s9RaY600UXFfGXi1cpJvMVOo3a/JWGyUWwgok/CM9I2BjLYemn+Jf52eZ4VmpwWN4YRjfS/zZZCVGLW4fUS/Y/jYe+/H6kWZYbRjacvFlfO2Zac+L4RDqkGsidVIeIbevgQXQK+qiDiEWVSJAxCIbNEWQJx/fQbkh+SG5RFscYkhtofkhuSIAaaEMdaEz5ZW+oANj0BHBlfva3MXAhkxJEGPSnfMjwYtMQ0RY8mLTG1F68hSCyxD8Wra5307DkWR4skxjknfXl0IsdCgACQAI2vnmumh2ck7WVskvF9RAAScM8y7pJ2RR4bU3Gx9n8dGEra8Xwk19B4qzHqMih2SNrwtUku7flcrJxL3eSlaal1RSSL32cjE7gmQ6kCFVgWU0Rpxv28RTRFFXVpkDFxXJWLesitxERWzTGJWTQ1JEmaGJoSx9ozJDckOsRMLJ2jLQloejC9+2Y0yAoTYVEEhUUBKQuIuJyKH4QVr3zvp2FZbE4mxyEG3ZHYQJEKYpahlQY9BD0aIShYhD2IQtJW53v5WsJir37K45GI1gI4RSpj8IZpj9Old3JEcqOYXCu67nqewaHBh6Ufw8X9TcvqYbZmFcpRitW1FeLdj0ynBJJLRKxoxLizh/6eW3GP8ASq3hocUOJaxfwMlJnoNampRcXo1YwmNouEpRa0diya8mXSztbSHNkaoOzkMyKrOhBEWqV2IRY1CDiEKzRArKoxIkVRidhC4jyESFyEc88u4AIYlipHIq/OwEBCbTujsRCFxAEPRHoIahLJq2vMfpa9RGWxJFOJKghimiVBEFljiGazVu9/gPMi1WAIbRJgiNDUmU4/vqCJbHqcfUnYemR6EC3wdG7RZFGfLOkXu7WEvNSekVxeei+d/I2BV7Fw3BC/N5+XL6+pZmuKpHmdTk35GwM7vNgrpVEu0vozQoRXpKUXF6NWJatFeOeySZ5lUyGJsstr4Rwk0+TKmbM0uGdzG1JWhE2RMRndkmTI9VEGiJV10RJk3EIiVJaK2nxFZaiPIbkOtXyQ28mADbEsVJiWArAWl5iZLPW/cUkAIegiRSI8GScOrvW3cRl0SbRiS4pfkM0Yj9iCbEzeREqTWls76kis7ENshjocoom00R6CVu9/gTKESURJk/CUzS7HwrbS6v9v5lRgKV7I22xcNwx4n5fU0442cjW5tsWWcIpJJcshYAaDhgAAAFFvFgeOPElmtTAYqLi+Hle9u56xUgmmnzMJvHs9xk2VZI+TpaLN9DMxKSG5zyty1Crk2loNuRRZ10iNiYW+ZX1EWdSNyuqoGOiM2NyHajvn+g1IgGNsTJ9RUjkpP0yAg4Lu3mxsWpPLtl4AQh+NsreZMw8efe1iHG2Xx7E7CoVlqLCjEdmdop5W1G60yCURcRMaUm7Lpp25nKs7sKZA5LoIs8JTvYg0VpkXmzaN2hooqyypF7sjC3aRsaUEkktErFVsXD2z6L4suTbCNI83qsm+dHQABjKAAAAcKrbWFU43LURVgmmnzIasaEtskzybamF4JN2yuVTefRfJG42/gvvIxWMoOLaZlnGmej0+RTiMzZDqQuyQ5WeoxUFNNEOaG5pW7/AEH5JZ3/AGyPMBWNMS1lr5C2ud/IQwFZwXF5Wt5iHLkKiAIfgWmAh1K2Es7rLwLrARvmyCwlpWIWIqWfXsTKk+FXRUV58++grJiN3JNN3tksvj4kVO/YmYaIDWWWGpvL1Rq9jYXST0KDZ1G7RutiYXTLLV/QtxxtnO1mbbEusLT4Ypc9X4j4AazgN27OgAAQAAAAAAAAU23cHxR44rNLPw6mC2jhbt8j1Qx28exZRvVpJyjq4pXce6XNfLwK5wvk36PUbXtb/R5ziYcLIs5X/fkXGN4WrmcxLaZmaO5GfHI+2ulxiURNPEXyY/wXzTAm0+iG0IZJnRYzKn6/QBWmNylfUXGLyvz07nFSfQVCHUASJWGTzsk8s+yvyL7BK0PEpMPTu8slrn0/MuPaqMSBmN4+qkrXzKac8xeMxF28yHCd3YEiXJLgnUi3wlO9srFfgaTZo8FShGzk0g2iSyUi52NhLtZZG8wlDhjbm82VWwdnuMVOUeHnGLVmvxNcn2L01QjSOBq82+VLo6AAWGQAAAAAAAAAAAADh0AAz+1t1cLXblKLpzes6bUW31aacW+7VzN1/svpyeWJqJdHCnJ+qsAEOKZbHPkiuJMY/wB09P8A5qf/AIofmIl9l1Rfcxi8JYb6qf0ACNkfYdavMvqKzG7i7RhdxjTrrlwVOGTXdTUV6NlHiNm4qDaqYWvHv7Cc4+Uopx+IAVvHE14dflbpkKbt96Eo+MJR+YU0392EpcsoSl8kAFW1HSWaVEzD7PxTdoYeu+6oVbf1cNkW1HdDalXL2MaMf8VSpBfCHFL4ABbGCOfn12ROlRYUvsprSV6mLhB81GhKov6nOPyHl9kzWmN9cNf/AOh0B1BGJ6vNf/RKw32aTi/exjcekcOoP1c5fI02xt08Nh2ppSq1FpUqNScf5UkoxfdK/cAGUI+wk9RkkuWaA6AElQAAAAAAAB//2Q==',
    'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxITEhUTEhMSFRUVFxUVGBcVFxUVFRcVFRUWFhUXFRUYHSggGBolHRUVITEhJSkrLi4uGB8zODMtNygtLisBCgoKDg0OGhAQGi0lHx8tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSstLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAMcA/QMBIgACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAABAUCAwYBB//EAEIQAAIBAgMFBQMJBwMEAwAAAAABAgMRBCExBRJBUWEGEyJxgTKRoQcjQlJyscHR8GKCkqKywuEUM/FDU2PSFnOT/8QAGgEBAAIDAQAAAAAAAAAAAAAAAAECAwQFBv/EAC8RAAIBAwIDBwQCAwEAAAAAAAABAgMEERIhBTFBUWFxgZHB0TKhsfAi4RNC8SP/2gAMAwEAAhEDEQA/APrPaLaEqFG8FHvJvu4b3sqbjJqUuiUW7ccl1KXsxOpRbdatVrOpuuTqO+60rPdSyir8EXfaXAyrUHGGcotSS5tar3NnLYDFKS1TT0t5Zo5V3cVKdeK6Y28epuUKcZQeTvgUuz9pbqUZ+zwfLo+haUcTCfsyT+/3M36VeFRZT8uprTpyjzNwAMxQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHjSPQAenCbQwrw+KlG3zdR79N8E5Zzh5qV3ytNI7srO0GGjOjJuO84eOPNNatelzTv6CrUWuq3Xl8mahPTNd5QN2WX5oU8Z3U01qs+md1b7yoxMFUtnOLjo4ScZdfToe1KjslvNtZZu9+d+p5yN3pisczouB2ez9t06sty0oyteztZ213WvxsWh82wuK3KkJvSM4t/ZUlf4X959JZ3uG3c7iDc+af77nPr0lBrHUAA6JgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB4AZAAA5HbuyYUn3kMoydnHgpW4dMn5HNRzdlq2kvXL8Tt+1s7UF1nH7mzidmv56H24f1xPLX1GEbxQjsnpfq8HSoTbpZfeYYjKWfE+g9ncX3uHg3rHwS845X9VZ+pxvaPC7lepHm3NeUs/z9xO7FY7dquk9KiuvtQ198fuRfhs3QunSl1zHzXL16eJWvHXT1LxO3AB6Y54AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANdetGEXKcoxjFXcpNJJc23oAbDw5KfyhYFVNzeq7t93ve7l3SfWWtutrdbHUqaaTTTTV09U09LPiVjOMvpeSXFrmjcACxBzfbWXzdNc6n4P8zj8HlUi/wDyU/61+R1nbGot6kvqqpJ+5Jficlh8mnylB+6SPK8Rmne+GF+H7nSoL/y9TrO2GEvKlUXHepv1V4/3HJ4acoSUo+1CSnHzXB9Hod52pheiuk4v71+JwWGq3m11Y4tFwu3Jdift7C23pLJ9OweJjUhGpHSSTXrwfVaG85fs3itx9234JPL9mT5dH+uJ1B6CzulcU1Nc+q7H8dV85Ro1abpywAAbRjAAAAAAAAAAAAAAAAAAAAAAAAABFxeMjTWebei4spUqRpxc5vCRMYuTwjLG4qNKO9K/RLVvkkcJtyrVxE/nH4U7xpr2Y9Xzl1fW1rlxXrSnLek7v4JckR4bru458L+R5a84jO4bjDaH3fj8euXy6dK2UFl7s47auCst1LJa+ZP7E7frYeE6L3qlOO66cbf7ae9eKf1clZcMyVtanrY87K7PcnVaX1P7yeHVJRrKMXjPxkmtBOO59NAOc25tl5wpuyWUpLVvlH8z0dzdU7eGufku39+xzqdOVR4RQdrcfvVZbuekV9lPN+ruRMLR3oZ8UbaOG35bz/XJIso0UeMqVXWk5vq2/X27DrRhpSRN25tJSoU45bzSnLpu3Xxl9xzuysKk2zLbEHG0uErLya4frke7Pq5F7m5nXqKc+xfb+8vzIp0lBYRZxikXeE2skvnHl9fglzl06+/mUMZkilItbXU6E9UH4ro/H92FSlGawzrwc9sXFOnPum/m5f7d/oy+ov2XwXDRZWS6E9fb3EK8NcP+PsOTUpuEsMAAzlAAAAAAAAAAAAAAAAAAAAVm0to7vhh7XF/V/wAmG4uIUIOc3t+e5fve9i9OnKbxEz2htBQ8Mc5fBefXoUTm5Nttt8WzyxlLJHkLu8qXMsy2S5Lovl9/pg61KjGmsLmRsReT3E7Raza1fRcjyq1CNllZWX4amyU7ZlVjcTyMKRlK2p4bpycpP2pPVv8AXA63sBH5urLnKMf4Vf8AuOEr1Xd6W4H0PsHh7YRS+vOcvc9z+06nCU5XOpdE3+F7mvevTSLbbGK3IWWssvJcWcdiY529S72xiN6rKP1Vb4Xf3lNXXE1eK1nVry7I7Ly5/f2LWtPTTXfueYd2JMZFfCTzsSaE89Pjxepz48jYaN+Mod5SlHjbLzWaObwlbidZSOa2jhtyrK2km5e/X4/G5lazEqtmTKdYnUKtyoosnUsjBHOS7Rbaou9nY3e8MvaXH6xQUJZG650LS7nbz1R3T5rtNarSVRYZ1IK/Z+O3vDL2v6iwPXUa0K0NcHt+7PvOTODg8MAAylQAAAAAAAAAAAADXVrRgrydl+tOZR47aEp5Lwx5cX5/kad3fU7Zfy3fRdX8Lvf35GWlRlU5cu0kbQ2n9Gm/OX4R/Mpo7288rRS1y8Tf3W/E2JHlRXVuB5O5uZ3E9c34LovD36vr0OtTpxpx0xNkbGFeSNblYi4isYsbFsbmrF1uCKfE1SViK6RUYqv1JyZEiJiJZ5Z35cT7FsnB93QpU3rCEU/tWW98bnzXsVge/wAZBteGl84/3X4P5rO3Rn1iR6PhFDEJTfXb0/fsc2/nlqPmcTKTdarf/uT/AKnY8xcDVtSq6WLnFp2bcr9ZWaXxJftLz5nCuI4qzT7X+Teh9MX3Ip55O+fHTibqMm0smvPX1NlSmYxkamnSZMllQZG2vh96F+Mc/Tj+uhIw0sjezKngx9TnMPElGqrT3JuPDVeT/VvQ3xLThtmJZM20atidTqXKvdJNOdjGshonFhhdqOOU/EufFefMqoVUbDYo3FShLVTeH+fH98DFOnGaxI6ajXjNXi0/1xXA2nLU5uLum0+hPo7WmvaW98H+R3bfjNOSxVWH2rdfK8N/E0alpJbx3LoFfT2tTet4+a/I3xxtJ/Tj65fedGF3Qn9M4+qNd0prnFkkGn/VQ+vH3o1S2jSX0l6Jv7kXlXpR3lNLxa+SFTm+j9GSwV89rQWik/SxDrbWm/ZW78X8cvgatTidtD/bPhv9+X3Mkbaq+nqXU5pK7aS5vIrsTtWKygt589I/5KqpVlJ+J38/y4GrO+mXM5NxxictqS0973fwvubVO0it5vP4/fQ2Vaspu8m7/d0Rhke7xg2cdqUm5S5s3EhvZidQ01KqSImJxFi2EicGzEYgqMTjFzI+OxxR4jFkqLk9ixNxGLvxIM6jk0km22kks229ElzIk653/wAnXZxu2LrL/wCqL/ra/p9/I3bW0lVnpXm+wx1asacdTOn7JbD/ANLRtK3eztKo1z4RT5K79W3xLxnp4eqhCMIqMeSOLKTk3J82cr2ow1q9OpwkrfvR/wANe41R0L7buF7yjJL2o+NecdV6q6Odw07o8zxWl/juNXSW/wA/PmdS0nqppdmxjWiQ5IsZRI1Smc2ccmyhhpk5Mr4qxLoTK4EjRtLC78br2o6deaK7DVi+KraeCedSGuslz6rqZIT6MqmbIme5yKzDYwsKddMu4IlM9SN1Ooa7jeKOOCSUqhmiJGaM1MrpIwSN0WNKqGSqDSwbbA174uSoEYNl0eb3Q17wdRE6AkZpnjZHnWRpniQ8InBJnUI9as+FvXh6cSHPFZ8NPiQcVjraBbklhXxSSzf4FJjcbwWXIKFar7EJNc9I/wATyJmG7Mt51Z+kP/Zr8BmK5g5jE4i5FVCctIv3Ze8+i0tk0aa8NNX97fm3myPWwHFq33llWx9KKyZxGyZ0qdaM8XTrOlB7zUIxkm08t/xezzSvfQ+7YDEQqU41KclKE4qUZLRxaumj4/teOVkrI6z5JsbJ0KuHksqM96L/AGaznLd6NSUvRo7vC7nU3TaS9zQuotrV2Henh6eHaNE9OMxFLuq04cL3j9mWat5aeh2ZzvavCu0K0fo+GX2W8n6N/E5nFaH+Whlc47+XX58jatJ6amH1/UQbnk4XNMKl0boSueXhLozq4NM4GuF1xJM4miSLSiCVTnkbCLCRuhMo0VaKvaOyb3lSyfGPB+XJ9CrhXlF2d01wep1diPi8HCovEvVZNepaM8cwVNLGm2OJTI+K2NOPsPeXukQJ70XaSafVWM6akC5VVGXeFKq7MlimQ6aJyXXfGUaxSrGGP+sI0YGToVWR7LEI5x441Txz5kYwDo54pEapjFzKBYqUnaN2+STb9yJtHZtef0d1c5O3wWZV7cyckmrjiK8VKTtFNvks2WWH2FBe3JyfJeFfn8Sfh4xi3CELJau1lpdWf0ijmugKihsmpLOb3Vy1l+SLHD7LpQz3bvnLxP8AJeiJ02oq8mklq27IhPakX/sp1XpdJqKei8bVtSmZSHPkTIvK+nnka6ldJtJOTWttF5vgV7wUqsr1nvbryjpFJ+XtZq2v5FhFRiskl5ZEqCQaPKEp6ztG69lZ283xNGMrLS6vy4mOKxlkUPzcPYil11bv1eZfCRGCPtHNs+jdjdjrDYeMWrVJ+Opz3novRWXvOc7IbM76r3kl4KbT+1PVL01fpzPoJ6DhFu1F1X12Xu/b/pzryazoXmDw9DO0aQNdSCknFq6aaaejT1RsABwmJpOjVlSeizi3xg9H+Hmmboy4l/2g2X39Pw2VSOcH98X0f5HIYTFu7jJNSi2mnqmsmmeP4hZO3qZj9L5fHl07jsW9b/JHvXMtkzXUhcwjM23NeMsrDM5GV0bYyMpRNEoENYJJUJmxMhRkbITIwmVaJVjGUE8mk/MwjMz3iNJGGRKuy6UvoJeV18FkRKmwYPSU152a+4tlM9cicyRBRvs+uFR/w/5Mf/jn/k/l/wAl635mQ1yJKOHZyHGc35WX4MkUNh0VrC7/AGm38NPgWMU1xv6IyuVbkxk1UqKirRiorkkkvgZ2PXI1zs9SNLZIdReb4cn5PiasPWnJ5x3UtHrvdPEk/Wxv3zyVYnSQaamHhKV5eK2dpZr3fmZzay6cFkjTUxBX1ce7tNWS0d07+nAksTa+KUVdvIrMbj8nZq/C+hBxe0Xoirr4luVrPO+fBBJvkW0k6vjG1rZmGFpzqzjTgryk7Lz5vklqQnM+k9iuz7oQ7yqrVZrJPWEOX2nx9FzNyztJXE9PRc/3tfQxV6qpRz6F7snARoUo048Fm/rSesmTQD10YqKSXJHEbbeWAwGSQAAADm+0uwHV+eo2VZLNaKolwfKXJ+j4NdIDFWowqwcJrKZeE5QlqifMsLjtYyvGSdmnk01qmnoywhXOg2/2ep4nxJ93VWk0tek19JfFfA4vFU62GluV425SWcZfZlx8teh5a74dUt3lbx7fns/HedajXjU8ez4LpTPd9FTSxafElRrJ62fuNNT7TNglzihEjuqeqsG0CVFmxyILrBYgnWiME3vAqpE78d6NSJwS+8XMKoQXV/X/ACN8jUME6VQwdQiRk768OXESqJatchkYJCxKejT8mmYyrZ28/gaXWSItXFeX4jIwTKlfqRK2LsQK2L1K+tiimW+RbST8TjXzKyviW+PnfkRqlcjTrGSNNkkmdU13bdldt5JLNtvRJE7YWw8Ri381G0L51JZQXPP6T6K/ofS+z/Zihhkmlv1ONSSz67i+ivj1Z0rbh9SrvyXa/bt/Hea9a5hT25vsKvsb2V7q1evH53WEHnudX+393np2YB6KjRjSjpjyORUqSqS1SAAMpQBgMAAAAAAAGjFYaFSLhUjGUXqpK6/56m8BrIOC212OqQvPDPfj/wBuT8S+xJ+15PPqzmVjZRbjNSjJZOMk4yXmnmj7GV21djUMRG1ampW0lpOP2ZLNeWhyLnhNOp/Kn/F/b+jdpXsltPf8/wBnzinjepvjibkna3YSvTvLDT72P1J2jU9JezL+U5avWqUpblaE6cuU4uL81fVdUcOtZVqX1I36daE/pZfyxBhKv1RTQxyfE3RxRquLXQylzCsZVMRuq7v6ZlOsUbFjAMFv3pj35USxxpnjepOGQXjxZpqYspZYzqap43qTokydi2qYvqRqmKKupjVzJmztl4rEZ0aM5Rf0mt2H8crJ+jMkLec3hJthyUVl7GqviSDWxiWrS8ztMH8m9abTr14048Y0lvyf78rKPuZ1Oy+xWBoNSVFVJrSdX5yV+aT8MX5JHVocKqP6tjVneU48tz5dsnYeLxdnQpPcf/Un4aduak/a/dud/sP5PqFO0sQ++nrbONJfu38X72XQ7UHWo2NKnvjL7zSqXdSfLZd3yYU6aSSSSSySWSS6IzANw1QAAAAAAGAAAAAAAAAAAAAADRicJCpHdqQjOL4TSkvcwADm8f2CwVS7jGdJvjTk7fwyul6I5/G/JtXX+xiISXKrFwa/ejvX9yPAa07SjPnFfgzRuKkepVYjsTtKGlOnU+xUj/fukCXZ3aK1w7//AEo/+56DUnw+insjYjdTaN1PsdtOf/Q3U+Lq0v7ZNk6HyeY96zw8fOc2/hBgGWlw+g+aMdS8qLlgmUfkxrv28VTj9mnKf3yiW2D+TPDRzq1a9R8rxhH+Vb38wBmjaUY8ooxu5qvqdFs/s1g6Fu6w9NNaSa35/wAc7v4luAbEUksLYxNt7sAAkgAAAAAAAAAAAAHjdgAD/9k=',
    'https://media.istockphoto.com/photos/mango-isolated-on-white-background-picture-id911274308?k=20&m=911274308&s=612x612&w=0&h=YY8-xqycxsqFea5B-JdhlcgExlXYWMiFoLJdQ-LUx5E=',
    'https://5.imimg.com/data5/VN/YP/MY-33296037/orange-600x600-500x500.jpg',
    'https://5.imimg.com/data5/GJ/MD/MY-35442270/fresh-pineapple-500x500.jpg'
  ];

  static List description = [
    'Apple is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
    'Banana is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
    'Mango is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
    'Orange is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
    'Pineapple is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry'
  ];

  final List<FruitDataModel> Fruitdata = List.generate(
      fruitname.length,
      (index) => FruitDataModel(
          '${fruitname[index]}', '${url[index]}', '${description[index]}'));

// Drawer
  late Image personAccountImage;
  late Image accountHeaderImage;

  @override
  void initState() {
    super.initState();

    personAccountImage = Image.asset("assets/images/cat.png");
    accountHeaderImage = Image.asset("assets/images/cat.png");
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    precacheImage(personAccountImage.image, context);
    precacheImage(accountHeaderImage.image, context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: ListView.builder(
        itemCount: Fruitdata.length,
        itemBuilder: (context, index) {
          return Container(
            height: 100,
            child: Card(
              child: Align(
                alignment: Alignment.center,
                child: ListTile(
                  title: Text(
                    Fruitdata[index].name,
                    style: TextStyle(
                      fontSize: 25,
                    ),
                  ),
                  leading: SizedBox(
                    width: 70,
                    height: 70,
                    child: Image.network(Fruitdata[index].ImageUrl),
                  ),
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => FruitDetail(
                              fruitDataModel: Fruitdata[index],
                            )));
                  },
                ),
              ),
            ),
          );
        },
      ),
      drawer: Drawer(
        child: ListView(padding: const EdgeInsets.all(0.0), children: <Widget>[
          UserAccountsDrawerHeader(
            accountName: const Text('Norapich Pongsuwan'),
            accountEmail: const Text('6250110004@psu.ac.th'),
            currentAccountPicture: CircleAvatar(
              backgroundImage: personAccountImage.image,
            ),
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: accountHeaderImage.image, fit: BoxFit.cover)),
          ),
          ListTile(
              title: const Text('About'),
              leading: const Icon(Icons.info),
              onTap: () {
                Navigator.of(context).pop();
              }),
          const Divider(),
          ListTile(
              title: const Text('Close'),
              leading: const Icon(Icons.close),
              onTap: () {
                Navigator.of(context).pop();
              }),
        ]),
      ),
    );
  }
}
